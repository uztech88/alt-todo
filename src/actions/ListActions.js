var alt = require('../alt');

class ListActions {
  updateLists(lists) {
    console.log("updateLocations action called");
    this.dispatch(lists);
  }

  fetchLists() {
    console.log("fetchLocations action called");
    this.dispatch();
  }


  listsFailed(errorMessage) {
    console.log("locationsFailed action called");
    this.dispatch(errorMessage);
  }
  completeList(list){
    console.log("Complete List action called");
    this.dispatch(list);
  }
  removeFromCompleteList(list){
    console.log("remove completed list action called");
    this.dispatch(list);
  }
  removeFromList(list){
    console.log("remove list action called");
    this.dispatch(list);
  }

  validationError(message){
    console.log("validation error action called");
    this.dispatch(message);
  }

  validationCompleteError(message){
    console.log("validation complete error action called");
    this.dispatch(message);
  }

  // favoriteLocation(location) {
  //   console.log("favoriteLocation action called");
  //   this.dispatch(location);
  // }

}

module.exports = alt.createActions(ListActions);
