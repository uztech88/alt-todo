var React = require('react');
var AltContainer = require('alt/AltContainer');
var ListStore = require('../stores/ListStore');
var CompletedStore = require('../stores/CompletedStore');
var ListActions = require('../actions/ListActions');


var Completed = React.createClass({
getRemoveButton(list){
  return (
      <button onClick = {this.remove} data-id = {list.id} >Remove</button>
    );
},
remove(ev){
    var list = CompletedStore.getList(Number(ev.target.getAttribute("data-id")));
    ListActions.removeFromCompleteList(list);
},
render(){

  if (this.props.validationMessage) {
      var ValidationError = <div> Error :: {this.props.validationMessage}</div>
    }

  if(this.props.lists.length > 0){
    return (
      <div>
      {ValidationError}
      <ul>
        {this.props.lists.map((list, i) => {

          return (
            <li key={i}>
              {list.value} {this.getRemoveButton(list)}
            </li>
          );
          
        })}
      </ul>
      </div>
      );
  }
  return (
      <div>
      {ValidationError}
      <p>No Completed Tasks</p>
      </div>
    );
}

});

var AllList = React.createClass({

  getRemoveButton(list){
    return <button onClick = {this.remove} data-id = {list.id} >Remove</button>
  },
  getButton(list){
  
   if(list.is_completed){
     return "  =>  Completed"
   }
   return <button onClick = {this.complete} data-id = {list.id} >Complete</button>
  },
  complete(ev){
      var list = ListStore.getList(Number(ev.target.getAttribute("data-id")));
      ListActions.completeList(list);
  },
  remove(ev){
    var list = ListStore.getList(Number(ev.target.getAttribute("data-id")));
      ListActions.removeFromList(list);
  },
  render() {

    if (this.props.validationMessage) {
      var ValidationError = <div> Error :: {this.props.validationMessage}</div>
    }

    console.log("render all Lists called");
    if (this.props.errorMessage) {
      return (
        <div>{this.props.errorMessage}</div>
      );
    }

    if (ListStore.isLoading()) {
      return (
        <div>
          <img src="ajax-loader.gif" />
        </div>
      )
    }
   
    if(this.props.lists.length > 0){
    return (
      <div>
      {ValidationError}
      <ul>

        {this.props.lists.map((list, i) => {

          return (
            <li key={i}>
              {list.value} {this.getButton(list)} {this.getRemoveButton(list)}
            </li>
          );
          
        })}
      </ul>
      </div>
      );
     }

     return (
      <div>
      {ValidationError}
      <p>No Tasks</p>
      </div>
    );


  } 
});

var List = React.createClass({
  componentDidMount() {
     ListStore.fetchLists();
  },
  addTask(){

    var element = document.getElementById("tasks");
    var { lists } = ListStore.getState();
    if(element.value && !ListStore.hasValue(element.value)){
    lists.push({"id" : ListStore.getNextid() , "value" : element.value});
    ListActions.updateLists(lists);
    element.value = "";
    }else{
      ListActions.validationError("Invalid Value or Value already exist");
    }
  },
  render() {
    console.log("render Lists called");
    return (
      <div>
        <h1>Lists</h1>
        <input type = "text" id = "tasks" placeholder = "enter tasks"/>
        <button onClick = {this.addTask}>Add</button>
        <AltContainer store = {ListStore} >
         <AllList />
        </AltContainer>
        <h4>Completed Tasks:</h4>
        <AltContainer store = {CompletedStore} >
         <Completed />
        </AltContainer>
      </div>
    );
  }

});

module.exports = List;
