var alt = require('../alt');
var ListActions = require('../actions/ListActions');
var ListSource = require('../sources/ListSource');

class ListStore {

  constructor() {
    this.lists = [];
    this.errorMessage = null;
    this.validationMessage = "";

    this.bindListeners({
      handleUpdatelists: ListActions.UPDATE_LISTS,
      handleFetchlists: ListActions.FETCH_LISTS,
      handleListsFailed: ListActions.LISTS_FAILED,
      addToCompleteList : ListActions.COMPLETE_LIST,
      removeList : ListActions.REMOVE_FROM_LIST,
      validationError : ListActions.VALIDATION_ERROR
    });

    this.exportPublicMethods({
      getList: this.getList,
      getNextid : this.getNextid,
      hasValue : this.hasValue
    });

    this.exportAsync(ListSource);
  }

  handleUpdatelists(lists) {
     console.log("handleUpdatelists called");
     console.log(lists);
    this.lists = lists;
    this.last_id = lists.length;
    this.errorMessage = null;
    this.validationMessage = "";
  }

  removeList(list){
    console.log("remove list from ListStore called");
    for (var i = 0; i < this.lists.length; i += 1) {
        if (this.lists[i].id === list.id) {
          this.lists.splice(i,1);
          break;
        }
    }
  }
  
  getNextid(){
    var { lists } = this.getState();
    if(lists.length > 0){
      return lists[lists.length-1].id + 1;
    }
    return 0;
  }

  handleFetchlists() {
    console.log("handleFetchLocations called");
    this.lists = [];
  }

  handleListsFailed(errorMessage) {
     console.log("handleLocationsFailed called");
     this.errorMessage = errorMessage;
  }

  addToCompleteList(list){

      this.removeList(list);
      console.log("addToCompleteList called from liststore");
      console.log(this.lists);
  }

  validationError(message){
    console.log("validation error in list store called");
    this.validationMessage = message;
  }

  hasValue(value){
    console.log(value);
    var { lists } = this.getState();
    for (var i = 0; i < lists.length; i += 1) {
      if (lists[i].value == value.toLowerCase()) {
        console.log("exists");
        return true;
      }
    }
    return false;
  }
  getList(id) {
    var { lists } = this.getState();
    for (var i = 0; i < lists.length; i += 1) {
      if (lists[i].id === id) {
        return lists[i];
      }
    }
    return null;
  }
}

module.exports = alt.createStore(ListStore, 'ListStore');
