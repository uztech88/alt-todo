var alt = require('../alt');
var ListActions = require('../actions/ListActions');

class CompletedStore {
  constructor() {
    this.lists = [];
    this.validationMessage = "";

    this.bindListeners({
      addCompletedList: ListActions.COMPLETE_LIST,
      removeList : ListActions.REMOVE_FROM_COMPLETE_LIST,
      validationError : ListActions.VALIDATION_COMPLETE_ERROR
    });

    this.exportPublicMethods({
      getList: this.getList
    });
  }

  addCompletedList(list) {
  	console.log("addCompletedList called from completestore");
    this.lists.push(list);
    console.log(this.lists);
    this.validationMessage = "";
  }

  validationError(message){
  	this.validationMessage = message;
  }

  removeList(list){
    console.log("remove list from ListStore called");
    for (var i = 0; i < this.lists.length; i += 1) {
        if (this.lists[i].id === list.id) {
          this.lists.splice(i,1);
          break;
        }
    }
  }
  getList(id) {
    var { lists } = this.getState();
    for (var i = 0; i < lists.length; i += 1) {
      if (lists[i].id === id) {
        return lists[i];
      }
    }
    return null;
  }
}

module.exports = alt.createStore(CompletedStore, 'CompletedStore');
