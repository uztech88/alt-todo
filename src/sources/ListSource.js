var ListActions = require('../actions/ListActions');

var mockData = [
{"id":0 ,"value":"test"}
];

var ListSource = {
  fetchLists() {
    console.log("fetchList called from source");
    return {
      remote() {
        return new Promise(function (resolve, reject) {
          // simulate an asynchronous flow where data is fetched on
          // a remote server somewhere.
          setTimeout(function () {

            // change this to `false` to see the error action being handled.
            if (true) {
              // resolve with some mock data
              resolve(mockData);
            } else {
              reject('Things have broken');
            }
          }, 250);

        });
      },

      local() {
        // Never check locally, always fetch remotely.
        return null;
      },

      success: ListActions.updateLists,
      error: ListActions.ListsFailed,
      loading: ListActions.fetchLists
    }
  }
};

module.exports = ListSource;
